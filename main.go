package main

import (
	"fmt"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/kristaponis/photoalbum/handlers"
	"gitlab.com/kristaponis/photoalbum/views"
	"gitlab.com/kristaponis/photoalbum/views/templates"
	"log"
	"net/http"
)

func main() {
	r := chi.NewRouter()
	r.Use(middleware.Logger)

	tmpl, err := views.ParseFS(templates.FS, "home.gohtml", "layouts/header-footer.gohtml")
	if err != nil {
		panic(err)
	}
	r.Get("/", handlers.StaticHandler(tmpl))

	tmpl, err = views.ParseFS(templates.FS, "contacts.gohtml", "layouts/header-footer.gohtml")
	if err != nil {
		panic(err)
	}
	r.Get("/contacts", handlers.StaticHandler(tmpl))

	tmpl, err = views.ParseFS(templates.FS, "signup.gohtml", "layouts/header-footer.gohtml")
	if err != nil {
		panic(err)
	}
	r.Get("/signup", handlers.StaticHandler(tmpl))

	r.NotFound(func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, "Page not found", http.StatusNotFound)
	})

	fmt.Println("Server started...")
	if err = http.ListenAndServe(":3000", r); err != nil {
		log.Fatal("error running server:", err)
	}
}

package handlers

import (
	"gitlab.com/kristaponis/photoalbum/views"
	"net/http"
)

func StaticHandler(tmpl views.View) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		tmpl.Execute(w, nil)
	}
}

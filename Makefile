build:
	@go build -o bin/photoalbum

run: build
	@./bin/photoalbum

test:
	go test -v ./..
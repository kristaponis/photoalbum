package views

import (
	"fmt"
	"html/template"
	"io/fs"
	"log"
	"net/http"
)

type View struct {
	template *template.Template
}

func (v View) Execute(w http.ResponseWriter, data interface{}) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")

	err := v.template.Execute(w, nil)
	if err != nil {
		log.Printf("executing template: %v", err)
		http.Error(w, "An error executing template", http.StatusInternalServerError)
		return
	}
}

func ParseFS(fs fs.FS, patterns ...string) (View, error) {
	tmpl, err := template.ParseFS(fs, patterns...)
	if err != nil {
		return View{}, fmt.Errorf("parsing fs template %w", err)
	}
	return View{
		template: tmpl,
	}, nil
}

func Parse(filePath string) (View, error) {
	tmpl, err := template.ParseFiles(filePath)
	if err != nil {
		return View{}, fmt.Errorf("parsing template %w", err)
	}
	return View{
		template: tmpl,
	}, nil
}
